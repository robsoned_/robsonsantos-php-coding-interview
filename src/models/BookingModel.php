<?php

namespace Src\models;

use Src\helpers\Helpers;
use Src\controllers\Dog as DogController;

class BookingModel {

	private $dogController;

	private $helpers;

	private const ENTITY_NAME = 'bookings';

	private const DISCOUNT_RATE = 10;

	function __construct() {
		$this->helpers = new Helpers();
		$this->dogController = new DogController();
	}

	public function getBookings() {
		return $this->helpers->getJson(self::ENTITY_NAME);
	}

	public function createBooking($booking) {
		$bookings = $this->getBookings();

		$booking['id'] = end($bookings)['id'] + 1;

		$booking = $this->addBookingDiscount($booking);

		$bookings[] = $booking;

		$this->helpers->putJson($bookings, 'bookings');

		return $booking;
	}

	private function addBookingDiscount($booking){

		$dogs = $this->dogController->getDogsByClientId($booking['clientid']);

		$dogsAverage= $this->getDogsAverageAge($dogs);

		$bookingPrice = $booking['price'];

		if ($dogsAverage < 10) {
			$discountMultiplier = self::DISCOUNT_RATE / 100;
			$booking['price'] = $bookingPrice - ($bookingPrice * $discountMultiplier);
		}

		return $booking;

	}


	private function getDogsAverageAge(array $dogs): float {

		$numDogs = count($dogs);

		$sumDogsAge = 0;

		foreach ($dogs as $dog) {
			$sumDogsAge = $sumDogsAge + $dog['age'];
		}

		return round($sumDogsAge / $numDogs);

	}

}