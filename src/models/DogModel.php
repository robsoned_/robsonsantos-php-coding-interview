<?php

namespace Src\models;

use Src\helpers\Helpers;

class DogModel {

	private $dogData;

	private $helper;

	private const ENTITY_NAME='dogs';

	function __construct() {
		$this->helper = new Helpers();
	}

	public function getDogs() {
		return $this->helper->getJson(self::ENTITY_NAME);
	}

	public function getDogsByClientId(int $clientId): array{

		$dogs = $this->getDogs();

		$resultDogs = [];

		foreach ($dogs as $dog) {
			if ($dog['clientid'] == $clientId) {
				$resultDogs[] = $dog;
			}
		}


		return $resultDogs;

	}
}