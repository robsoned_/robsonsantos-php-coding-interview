<?php

namespace Src\helpers;

class Helpers {
	function putJson($data, $entity) {
		file_put_contents(dirname(__DIR__) . '/../scripts/'.$entity.'.json', json_encode($data, JSON_PRETTY_PRINT));
	}
	public function arraySearchI($needle, $haystack, $column) {
		return array_search($needle, array_column($haystack, $column));
	}

	public function getJson(string $entity){
		$data = file_get_contents(dirname(__DIR__) . '/../scripts/'.$entity. '.json');

		return json_decode($data,true);
	}
}